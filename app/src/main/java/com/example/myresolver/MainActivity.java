package com.example.myresolver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button=findViewById(R.id.button);
        TextView textView=findViewById(R.id.textView);

        ContentResolver contentResolver=getContentResolver();

        Uri uri=Uri.parse("content://plf.provider1/student");

        ContentValues contentValues=new ContentValues();
        contentValues.put("name","plf");
        contentValues.put("age",21);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contentResolver.insert(uri,contentValues);
            }
        });
    }

}